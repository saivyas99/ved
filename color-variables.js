export const $primary_color = "#24292e";
export const $secondary_color = "#24292e";
export const $badge_background_color = "#45494d";
export const $font_color = "#fff";
export const $link_color = "#136cd1";