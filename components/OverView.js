import React, { Component } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Badge, Text, Icon } from "native-base"
import { listRepos } from '../reducer';
import * as color_vars from "../color-variables";
import OCTICON from "react-native-vector-icons/Octicons";
var GitHubColors = require("github-colors");

class OverView extends Component {
	static navigationOptions = ({ navigation, screenProps }) => ({
		tabBarLabel: <View style={{ flex: 1, flexDirection: "row" }}>
			<Text style={{ color: color_vars.$font_color, fontSize: 18 }}>Overview</Text>
		</View>,

	});
	componentDidMount() {
		this.props.listRepos('supreetsingh247');
	}
	renderItem = ({ item }) => (
		<View style={styles.item} >
			<View style={{ flexDirection: "row" }}>
				<OCTICON name="repo" style={{ fontSize: 20, marginRight: 10 }} />

				<Text style={{ color: color_vars.$link_color, fontSize: 20 }}>
					{item.full_name}</Text>
			</View>
			<View >
				<Text style={{ marginLeft: 25, color: color_vars.$badge_background_color, marginTop: 10, marginBottom: 5 }}>{item.description != null ? item.description.length > 75 ? item.description.substring(0, 75) + "..." : item.description : ""}</Text>
			</View>
			<View style={{ marginLeft: 25, flexDirection: "row" }}>
				<Icon name="md-star" style={{ fontSize: 20, color: color_vars.$primary_color }} />
				<Text style={{ marginLeft: 5, lineHeight: 20, fontSize: 15 }}>{item.stargazers_count}</Text>
				{item.language != null ?
					<View style={{ flexDirection: "row" }}>
						<View style={{ backgroundColor: GitHubColors.get(item.language).color, marginTop: 2, marginLeft: 20, marginRight: 5, width: 15, height: 15, borderRadius: 7 }}>
							<Text></Text>
						</View>
						<Text style={{ fontSize: 15, color: color_vars.$badge_background_color }}>{item.language}</Text>

					</View>
					: null
				}
			</View>
		</View>
	);
	render() {
		const { repos } = this.props;
		return (
			<FlatList
				styles={styles.container}
				data={repos}
				renderItem={this.renderItem}
			/>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	item: {
		flexDirection: "column",
		flex: 1,
		minHeight: 130,
		padding: 16,
		borderBottomWidth: 1,
		borderBottomColor: '#ccc'
	}
});

const mapStateToProps = state => {
	let storedRepositories = state.repos.map(repo => ({ key: repo.id, ...repo }));
	return {
		repos: storedRepositories
	};
};

const mapDispatchToProps = {
	listRepos
};

export default connect(mapStateToProps, mapDispatchToProps)(OverView);