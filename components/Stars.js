import React, { Component } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Badge, Text } from "native-base"
import { listRepos } from '../reducer';
import * as color_vars from "../color-variables"

class Stars extends Component {
	static navigationOptions = ({ navigation, screenProps }) => ({
		tabBarLabel: <View style={{ flex: 1, flexDirection: "row" }}>
			<Text style={{ color: color_vars.$font_color, fontSize: 18 }}>Stars</Text>
			<Badge style={{ backgroundColor: color_vars.$badge_background_color, marginLeft: 5 }}>
				<Text>6</Text>
			</Badge>
		</View>,

	});
	componentDidMount() {
		this.props.listRepos('supreetsingh247');
	}
	renderItem = ({ item }) => (
		<View style={styles.item}>
			<Text>{item.name}</Text>
		</View>
	);
	render() {
		const { repos } = this.props;
		return (
			<FlatList
				styles={styles.container}
				data={repos}
				renderItem={this.renderItem}
			/>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	item: {
		padding: 16,
		borderBottomWidth: 1,
		borderBottomColor: '#ccc'
	}
});

const mapStateToProps = state => {
	let storedRepositories = state.repos.map(repo => ({ key: repo.id, ...repo }));
	return {
		repos: storedRepositories
	};
};

const mapDispatchToProps = {
	listRepos
};

export default connect(mapStateToProps, mapDispatchToProps)(Stars);