import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar, Platform, Image } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { Provider, connect } from 'react-redux';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import { Button, Icon } from "native-base";
import reducer from './reducer';
import { createStackNavigator, createMaterialTopTabNavigator, createAppContainer, createDrawerNavigator } from 'react-navigation';
import * as color_vars from "./color-variables";
// Components
import RepoList from './components/RepoList';
import OverView from './components/OverView';
import Stars from './components/Stars';
import { Font } from 'expo';

const client = axios.create({
	baseURL: 'https://api.github.com',
	responseType: 'json'
});
let tabNav = createMaterialTopTabNavigator(
	{
		"Overview": {
			screen: OverView
		},
		"Repositories": {
			screen: RepoList
		},
		"Stars": {
			screen: Stars
		},
	}, {
		tabBarPosition: "top",
		initialRouteName: "Repositories",
		tabBarOptions: {
			scrollEnabled: true,

			labelStyle: {
				fontSize: 12,
			},

			style: {
				backgroundColor: color_vars.$primary_color
			},
			indicatorStyle: {
				opacity: 0

			}
		}
	});
let RootStack = createStackNavigator({
	Home: {
		screen: tabNav
	}
},
	{
		defaultNavigationOptions: ({ navigation, screenProps }) => ({
			headerTitle: <Icon style={{ color: "#fff" }} name="logo-github"></Icon>,
			headerStyle: {
				backgroundColor: color_vars.$primary_color,
				borderBottomWidth: 0,
			},
			headerTintColor: '#fff',
			headerTitleStyle: {
				fontWeight: 'bold',
			},
			headerLeft: <Button transparent onPress={() => navigation.openDrawer()} style={{ width: 100 }}><Icon name="md-menu" style={{ color: "#fff", marginLeft: 5, fontSize: 20, padding: 10 }} /></Button>,

		}),
	});
const MyDrawerNavigator = createDrawerNavigator({
	Home: {
		screen: RootStack,
	}
});
let Navigation = createAppContainer(MyDrawerNavigator);


const store = createStore(reducer, applyMiddleware(axiosMiddleware(client)));
const MyStatusBar = ({ backgroundColor, ...props }) => (
	<View style={[styles.statusBar, { backgroundColor, top: 0, left: 0 }]}>
		<StatusBar translucent backgroundColor={backgroundColor} {...props} />
	</View>
);
export default class App extends Component {
	constructor(props) {
		super(props);
		this.state = { loading: true };

	}
	async componentWillMount() {
		await Font.loadAsync({
			"Octicons": require("react-native-vector-icons/Fonts/Octicons.ttf"),
			"Ionicons": require("react-native-vector-icons/Fonts/Ionicons.ttf")
		});
		this.setState({ loading: false });
	}
	render() {
		if (this.state.loading) {
			return (<Text>Loading..</Text>)


		} else {
			return (

				<Provider store={store}>

					<View style={styles.container}>
						<MyStatusBar backgroundColor={color_vars.$primary_color} barStyle="light-content" />

						<Navigation screenProps={{ headerStyle: { backgroundColor: "red" }, headerTitleStyle: { textAlign: "center", alignSelf: "center", color: "#fff" }, iconColor: { color: "#fff" } }} backgroundColor={"red"} />
					</View>
				</Provider>
			);
		}

	}
}
const STATUSBAR_HEIGHT = Platform.OS === "ios" ? 0 : StatusBar.currentHeight;

const styles = StyleSheet.create({
	container: {
		flex: 1
	}
});